import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import logo from './logo.svg';
import Greetings from './components/Greetings';
import Input from './components/Input';
import Button from './components/Button';

ReactDOM.render(
  <React.StrictMode>
    <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" width="200px" />
      <Greetings />
      <Input placeholder="Username"/>
      <Input placeholder="Password"/>
      <Button />
    </header>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
