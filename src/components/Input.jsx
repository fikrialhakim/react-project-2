import React from 'react'
import './style.css'

class Input extends React.Component{
    render(){
        return(
            <input className="input" placeholder={this.props.placeholder}/>
        )
    }
}

export default Input